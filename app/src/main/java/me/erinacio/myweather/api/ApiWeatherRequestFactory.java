package me.erinacio.myweather.api;

import java.util.HashMap;

public class ApiWeatherRequestFactory extends RequestFactory {
    private static final HashMap<String, Object> preludes = new HashMap<String, Object>() {
        {
            put("key", "74b38751142a9429354ba633d6e2be62");
        }
    };
    //private static final String prefix = "http://10.0.2.2:5000/";
    private static final String prefix = "https://v.juhe.cn/";

    public ApiWeatherRequestFactory() {
        super(prefix, preludes);
    }
}
