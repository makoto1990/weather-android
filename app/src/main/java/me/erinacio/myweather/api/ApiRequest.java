package me.erinacio.myweather.api;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class ApiRequest extends Request<ApiResponse> {
    private static final String TAG = "MyWeather";

    private static String urlEncode(String s) throws UnsupportedEncodingException {
        return URLEncoder.encode(s, "utf-8");
    }

    private final Response.Listener<ApiResponse> listener;

    private ApiRequest(String url,
                       Response.Listener<ApiResponse> listener,
                       Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = listener;
    }

    public static ApiRequest makeRequest(String baseUrl,
                                         Map<String, ?> params,
                                         Response.Listener<ApiResponse> listener,
                                         Response.ErrorListener errorListener) {
        if (params.isEmpty()) {
            return new ApiRequest(baseUrl, listener, errorListener);
        }

        StringBuilder sb = new StringBuilder(baseUrl);
        if (baseUrl.contains("?")) {
            if (!baseUrl.endsWith("?")) {
                sb.append('&');
            }
        } else {
            sb.append('?');
        }
        try {
            for (Map.Entry<String, ?> param : params.entrySet()) {
                sb.append(urlEncode(param.getKey()));
                sb.append('=');
                sb.append(urlEncode(String.valueOf(param.getValue())));
                sb.append('&');
            }
            sb.setLength(sb.length() - 1);
            String url = sb.toString();
            Log.d(TAG, String.format("ApiRequest.makeRequest: url is %s", url));
            return new ApiRequest(sb.toString(), listener, errorListener);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Response<ApiResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Gson gson = new Gson();
            return Response.success(gson.fromJson(json, ApiResponse.class),
                                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException | JsonParseException e) {
            Log.w(TAG, "parseNetworkResponse: it sucks", e);
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(ApiResponse response) {
        listener.onResponse(response);
    }

    public Request<ApiResponse> addTo(RequestQueue requestQueue) {
        return requestQueue.add(this);
    }
}
