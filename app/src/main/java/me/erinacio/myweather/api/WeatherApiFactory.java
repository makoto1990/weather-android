package me.erinacio.myweather.api;

import com.android.volley.Response;

import java.util.HashMap;

public class WeatherApiFactory {
    private static final String urlWeather = "/weather/index";
    private RequestFactory requestFactory;

    public WeatherApiFactory(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    public ApiRequest weatherRequest(String cityName,
                                     Response.Listener<ApiResponse> listener,
                                     Response.ErrorListener errorListener) {
        return requestFactory.makeApiRequest(urlWeather, new HashMap<String, Object>() {
            {
                put("cityname", cityName);
                put("format", 2);
            }
        }, listener, errorListener);
    }
}
