package me.erinacio.myweather;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import me.erinacio.myweather.fragments.dashboard.DashboardItem;

public class DashboardContentActivity extends AppCompatActivity {


    private DashboardItem dashboardItem;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(0);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_content);
        Intent intent = getIntent();
        if (intent != null) {
            this.dashboardItem = (DashboardItem) intent.getSerializableExtra("dashboardItem");
        }
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        TextView dashboardTitle = findViewById(R.id.dashboard_content_title);
        dashboardTitle.setText(dashboardItem.getTitle());
        TextView dashboardContent = findViewById(R.id.dashboard_content_content);
        dashboardContent.setText(dashboardItem.getContent());

    }
}
