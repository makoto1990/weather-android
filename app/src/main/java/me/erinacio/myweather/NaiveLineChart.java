package me.erinacio.myweather;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

public class NaiveLineChart extends View {
    private static int pointRadius;

    private Paint linePaint;
    private int width;
    private int height;
    private int layoutWidth;
    private int layoutHeight;
    private int left;
    private int right;
    private int top;
    private int bottom;

    private void init(int color) {
        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setColor(color);
        linePaint.setStrokeWidth(2);
    }

    public NaiveLineChart(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        pointRadius = 5;
        init(Color.GRAY);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (data.length > 0) {
            canvas.drawLines(points, 0, data.length * 4 - 4, linePaint);
        }
        for (int i = 0; i < points.length; i += 4) {
            canvas.drawCircle(points[i], points[i + 1], pointRadius, linePaint);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        layoutWidth = w;
        layoutHeight = h;
        left = getPaddingLeft();
        right  = w - getPaddingRight() - 1;
        top = getPaddingTop();
        bottom = h - getPaddingBottom() - 1;
        width  = right - left + 1;
        height = bottom - top + 1;

        recalculatePoints();
    }

    private void recalculatePoints() {
        if (data.length == 0) {
            points = new float[0];
            return;
        }
        points = new float[data.length * 4 - 2];

        double min = data[0], max = data[0];

        for (double datum : data) {
            if (min > datum) min = datum;
            if (max < datum) max = datum;
        }

        double ratioX = (double) (width) / data.length;
        double ratioY;
        if (max != min) {
            ratioY = (height - 2 * pointRadius) / (max - min);
        } else {
            ratioY = 0;
        }
        double offsetX = ratioX / 2.0 + left;
        double offsetY = top + pointRadius;

        for (int i = 0, j = 0; i < data.length; ++i, j += 4) {
            points[j] = (float) (i * ratioX + offsetX);
            points[j + 1] = (float) ((max - data[i]) * ratioY + offsetY);
        }
        for (int i = 2; i < points.length; i += 4) {
            points[i] = points[i + 2];
            points[i + 1] = points[i + 3];
        }
    }

    private double[] data = new double[]{};
    private float[] points = new float[]{};

    public double[] getData() {
        return data;
    }

    public void setData(double[] data) {
        if (data == null) {
            throw new NullPointerException();
        }
        this.data = data;

        recalculatePoints();
        invalidate();
    }

    public int spToPixel(float sp, Context context) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
        return px;
    }
}
