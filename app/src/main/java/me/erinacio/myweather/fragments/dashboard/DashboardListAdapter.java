package me.erinacio.myweather.fragments.dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import me.erinacio.myweather.R;

/**
 * Created by kevin on 2017/12/25.
 */

public class DashboardListAdapter extends BaseAdapter {
    private List<DashboardItem> dashboardItemList;
    private LayoutInflater inflater;

    public DashboardListAdapter() {
    }

    public DashboardListAdapter(List<DashboardItem> stuList, Context context) {
        this.dashboardItemList = stuList;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return dashboardItemList == null ? 0 : dashboardItemList.size();
    }

    @Override
    public DashboardItem getItem(int position) {
        return dashboardItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.dashboard_item, parent, false);
        DashboardItem dashboardItem = getItem(position);
        TextView dashboard_title = view.findViewById(R.id.dashboard_title);
        TextView dashboard_date = view.findViewById(R.id.dashboard_date);
        TextView dashboard_outline = view.findViewById(R.id.dashboard_outline);
        dashboard_title.setText(dashboardItem.getTitle());
        dashboard_date.setText(dashboardItem.getDate());
        dashboard_outline.setText(dashboardItem.getContent());
        return view;
    }
}
