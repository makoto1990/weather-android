package me.erinacio.myweather.fragments.weather;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import me.erinacio.myweather.R;
import me.erinacio.myweather.RequestQueueSingleton;
import me.erinacio.myweather.api.ApiWeatherRequestFactory;
import me.erinacio.myweather.api.RequestFactory;
import me.erinacio.myweather.vm.WeatherViewModule;

/**
 * Created by kevin on 2017/12/19.
 */

public class WeatherRoot extends Fragment {
    private RequestFactory requestFactory;
    private RequestQueue requestQueue;

    public static WeatherRoot newInstance() {
        WeatherRoot fragment = new WeatherRoot();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        requestFactory = new ApiWeatherRequestFactory();
        requestQueue = RequestQueueSingleton.getInstance(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.weather, container, false);
        new WeatherViewModule(view, requestFactory, requestQueue).updateWeather();
        return view;
    }

}
