package me.erinacio.myweather.api;

import java.util.HashMap;

public class ApiAqiRequestFactory extends RequestFactory {
    private static final HashMap<String, Object> preludes = new HashMap<String, Object>() {
        {
            put("key", "f341b719e99740cc7ac352db5c114a33");
        }
    };
    //private static final String prefix = "http://10.0.2.2:5000/";
    private static final String prefix = "http://web.juhe.cn:8080/";

    public ApiAqiRequestFactory() {
        super(prefix, preludes);
    }
}