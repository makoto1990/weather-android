package me.erinacio.myweather.fragments.calendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Calendar;

import me.erinacio.myweather.R;

/**
 * Created by kevin on 2017/12/19.
 */

public class CalendarRoot extends Fragment {
    public static CalendarRoot newInstance() {
        CalendarRoot fragment = new CalendarRoot();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.calendar, container, false);
        MaterialCalendarView materialCalendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        materialCalendarView.setPagingEnabled(false);
        materialCalendarView.setSelectedDate(Calendar.getInstance());
        return view;
    }
}
