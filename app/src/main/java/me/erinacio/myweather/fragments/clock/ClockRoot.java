package me.erinacio.myweather.fragments.clock;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.erinacio.myweather.R;

/**
 * Created by kevin on 2017/12/19.
 */

public class ClockRoot extends Fragment {
    public static ClockRoot newInstance() {
        ClockRoot fragment = new ClockRoot();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.clock, container, false);
        return view;
    }
}
