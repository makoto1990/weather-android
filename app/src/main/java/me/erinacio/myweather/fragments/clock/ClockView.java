package me.erinacio.myweather.fragments.clock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews.RemoteView;

import java.util.TimeZone;

import me.erinacio.myweather.R;

/**
 * Created by kevin on 2017/12/22.
 */
@RemoteView
public class ClockView extends View {

    private static int hour;
    private static int minute;
    private static int second;
    private static String debug = "ClockView";
    private static int SECONDS_FLAG = 0;
    private Time mCalendar;
    private Drawable mHourHand;
    private Drawable mMinuteHand;
    private Drawable mDial;
    private Paint mPaint = new Paint();
    private Rect boundsTime = new Rect();
    private int mDialWidth;
    private int mDialHeight;
    private boolean mAttached;
    private boolean mSingleSecond;
    private float mMinutes;
    private float mHour;
    private boolean mChanged;
    private Message secondsMsg;
    private float mSeconds;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    onTimeChanged();// 重新获取的系统的当前时间，得到时，分，秒  
                    invalidate();// 强制绘制，调用自身的onDraw();  
                    break;

                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(debug, "onReceive");
            Log.e(debug, "intent action=" + intent.getAction());
            if (intent.getAction().equals(Intent.ACTION_TIMEZONE_CHANGED)) {
                String tz = intent.getStringExtra("time-zone");
                mCalendar = new Time(TimeZone.getTimeZone(tz).getID());
            }
            onTimeChanged();
            invalidate();
        }
    };

    public ClockView(Context context) {
        this(context, null);
    }

    public ClockView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public ClockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Resources r = context.getResources();

        if (mDial == null) {
            mDial = r.getDrawable(R.drawable.clock_dial);
        }

        if (mHourHand == null) {
            mHourHand = r.getDrawable(R.drawable.clock_hand_hour);
        }

        if (mMinuteHand == null) {
            mMinuteHand = r.getDrawable(R.drawable.clock_hand_minute);
        }

        mCalendar = new Time();

        mDialWidth = mDial.getIntrinsicWidth();
        mDialHeight = mDial.getIntrinsicHeight();

    }

    @Override
    protected void onAttachedToWindow() {
        Log.e(debug, "onAttachedToWindow");
        super.onAttachedToWindow();

        if (!mAttached) {
            mAttached = true;
        }
        mSingleSecond = true;

        mCalendar = new Time();

        onTimeChanged();

        initSecondsThread();
    }

    private void initSecondsThread() {
        secondsMsg = mHandler.obtainMessage(SECONDS_FLAG);
        Thread newThread = new Thread() {
            @Override
            public void run() {
                while (mAttached) {
                    secondsMsg = mHandler.obtainMessage(SECONDS_FLAG);
                    mHandler.sendMessage(secondsMsg);
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        };
        newThread.start();

    }

    @Override
    protected void onDetachedFromWindow() {
        Log.e(debug, "onDetachedFromWindow");
        super.onDetachedFromWindow();
        if (mAttached) {
            mAttached = false;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.e(debug, "onMeasure");
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        float hScale = 1.0f;
        float vScale = 1.0f;

        if (widthMode != MeasureSpec.UNSPECIFIED && widthSize < mDialWidth) {
            hScale = (float) widthSize / (float) mDialWidth;
        }

        if (heightMode != MeasureSpec.UNSPECIFIED && heightSize < mDialHeight) {
            vScale = (float) heightSize / (float) mDialHeight;
        }

        float scale = Math.min(hScale, vScale);

        setMeasuredDimension(resolveSize((int) (mDialWidth * scale),
                widthMeasureSpec), resolveSize((int) (mDialHeight * scale),
                heightMeasureSpec));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        Log.e(debug, "onSizeChanged");
        super.onSizeChanged(w, h, oldw, oldh);
        mChanged = true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.e(debug, "canvas");
        boolean changed = mChanged;
        if (changed) {
            mChanged = false;
        }

        int availableWidth = getWidth();
        int availableHeight = getHeight() * 2 / 3;

        int x = availableWidth / 2;
        int y = availableHeight / 2;

        final Drawable dial = mDial;
        int w = dial.getIntrinsicWidth();
        int h = dial.getIntrinsicHeight();

        boolean scaled = false;

        if (availableWidth < w || availableHeight < h) {
            scaled = true;
            float scale = Math.min((float) availableWidth / (float) w,
                    (float) availableHeight / (float) h);
            canvas.save();
            canvas.scale(scale, scale, x, y);
        }

        if (availableWidth > w || availableHeight > h) {
            scaled = true;
            float scale = Math.min((float) availableWidth / (float) w,
                    (float) availableHeight / (float) h);
            canvas.save();
            canvas.scale(scale, scale, x, y);
        }

        if (changed) {
            dial.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y + (h / 2));
        }
        dial.draw(canvas);

        canvas.save();
        canvas.rotate(mHour / 12.0f * 360.0f, x, y);

        final Drawable hourHand = mHourHand;
        if (changed) {
            w = hourHand.getIntrinsicWidth();
            h = hourHand.getIntrinsicHeight();
            hourHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y
                    + (h / 2));
        }
        hourHand.draw(canvas);
        canvas.restore();

        canvas.save();
        canvas.rotate(mMinutes / 60.0f * 360.0f, x, y);

        final Drawable minuteHand = mMinuteHand;
        if (changed) {
            w = minuteHand.getIntrinsicWidth();
            h = minuteHand.getIntrinsicHeight();
            minuteHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y
                    + (h / 2));
        }
        minuteHand.draw(canvas);
        canvas.restore();

        canvas.save();
        canvas.rotate(mSeconds / 60.0f * 360.0f, x, y);

        final Drawable secondHand = mMinuteHand;
        if (changed) {
            w = secondHand.getIntrinsicWidth();
            h = secondHand.getIntrinsicHeight();
            secondHand.setBounds(x - (w / 2), y - (h / 2), x + (w / 2), y
                    + (h / 2));
        }
        secondHand.draw(canvas);
        canvas.restore();

        int xText = getWidth() / 2;
        int yText = getHeight() * 5 / 6;

        int outputHour = (hour >= 12) ? hour - 12 : hour;

        String strTime = ((outputHour >= 10) ? Integer.toString(outputHour) : ("0" + Integer.toString(outputHour))) + ((mSingleSecond) ? ":" : " ") + ((minute >= 10) ? Integer.toString(minute) : ("0" + Integer.toString(minute)));
        //String strTime = Integer.toString(getHeight());
        mPaint.setDither(true);
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.parseColor("#7f7f7f"));
        mPaint.setTextSize((getHeight() / 10));
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.getTextBounds(strTime, 0, strTime.length(), boundsTime);
        Paint.FontMetricsInt fontMetrics = mPaint.getFontMetricsInt();
        int baseline = yText - 2 * boundsTime.height() - (fontMetrics.top + fontMetrics.bottom) - (int) mPaint.getTextSize() - fontMetrics.bottom;
        canvas.drawText(strTime, xText, baseline, mPaint);

        int xTerm = xText - boundsTime.width() / 2 - fontMetrics.leading - 6;
        String strTerm = (hour >= 12) ? "PM" : "AM";
        mPaint.setTextSize((getHeight() / 30));
        mPaint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(strTerm, xTerm, baseline, mPaint);

        if (scaled) {
            canvas.restore();
        }
    }

    private void onTimeChanged() {
        Log.e(debug, "onTimeChanged");
        mCalendar.setToNow();
        mSingleSecond = !mSingleSecond;

        hour = mCalendar.hour;
        minute = mCalendar.minute;
        second = mCalendar.second;

        mSeconds = second;
        mMinutes = minute + second / 60.0f;
        mHour = hour + mMinutes / 60.0f;

        mChanged = true;
    }

}
