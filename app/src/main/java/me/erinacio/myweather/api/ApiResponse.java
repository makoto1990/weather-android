package me.erinacio.myweather.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

public class ApiResponse {
    @SerializedName("resultcode")
    private String resultCode;

    @SerializedName("reason")
    private String reason;

    @SerializedName("result")
    private JsonElement result;

    @SerializedName("error_code")
    private int errorCode;

    private ApiResponse() {}

    public String getResultCode() {
        return resultCode;
    }

    public String getReason() {
        return reason;
    }

    public JsonElement getResult() {
        return result;
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return "ApiResponse" + (new Gson()).toJson(this);
    }
}
