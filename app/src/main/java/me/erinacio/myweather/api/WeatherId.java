package me.erinacio.myweather.api;

import java.util.HashMap;

import me.erinacio.myweather.R;

public class WeatherId {
    private static final HashMap<String, String> stringLookup = new HashMap<String, String>() {
        {
            put("00", "晴");
            put("01", "多云");
            put("02", "阴");
            put("03", "阵雨");
            put("04", "雷阵雨");
            put("05", "雷阵雨伴有冰雹");
            put("06", "雨夹雪");
            put("07", "小雨");
            put("08", "中雨");
            put("09", "大雨");
            put("10", "暴雨");
            put("11", "大暴雨");
            put("12", "特大暴雨");
            put("13", "阵雪");
            put("14", "小雪");
            put("15", "中雪");
            put("16", "大雪");
            put("17", "暴雪");
            put("18", "雾");
            put("19", "冻雨");
            put("20", "沙尘暴");
            put("21", "小雨-中雨");
            put("22", "中雨-大雨");
            put("23", "大雨-暴雨");
            put("24", "暴雨-大暴雨");
            put("25", "大暴雨-特大暴雨");
            put("26", "小雪-中雪");
            put("27", "中雪-大雪");
            put("28", "大雪-暴雪");
            put("29", "浮尘");
            put("30", "扬沙");
            put("31", "强沙尘暴");
            put("53", "霾");
        }
    };

    private static final HashMap<String, Integer> drawableLookup = new HashMap<String, Integer>() {
        {
            put("00", R.drawable.ic_weather_00);
            put("01", R.drawable.ic_weather_01);
            put("02", R.drawable.ic_weather_02);
            put("03", R.drawable.ic_weather_03);
            put("04", R.drawable.ic_weather_04);
            put("05", R.drawable.ic_weather_05);
            put("06", R.drawable.ic_weather_06);
            put("07", R.drawable.ic_weather_07);
            put("08", R.drawable.ic_weather_08);
            put("09", R.drawable.ic_weather_09);
            put("10", R.drawable.ic_weather_10);
            put("11", R.drawable.ic_weather_11);
            put("12", R.drawable.ic_weather_12);
            put("13", R.drawable.ic_weather_13);
            put("14", R.drawable.ic_weather_14);
            put("15", R.drawable.ic_weather_15);
            put("16", R.drawable.ic_weather_16);
            put("17", R.drawable.ic_weather_17);
            put("18", R.drawable.ic_weather_18);
            put("19", R.drawable.ic_weather_19);
            put("20", R.drawable.ic_weather_20);
            put("21", R.drawable.ic_weather_21);
            put("22", R.drawable.ic_weather_22);
            put("23", R.drawable.ic_weather_23);
            put("24", R.drawable.ic_weather_24);
            put("25", R.drawable.ic_weather_25);
            put("26", R.drawable.ic_weather_26);
            put("27", R.drawable.ic_weather_27);
            put("28", R.drawable.ic_weather_28);
            put("29", R.drawable.ic_weather_29);
            put("30", R.drawable.ic_weather_30);
            put("31", R.drawable.ic_weather_31);
            put("53", R.drawable.ic_weather_53);
        }
    };

    public static String weatherIdToString(String weatherId) {
        String desc = stringLookup.get(weatherId);
        if (desc == null) {
            desc = "未知";
        }
        return desc;
    }

    public static String widPairToString(WeatherResult.WeatherIdPair widPair) {
        String fa = widPair.getFa(), fb = widPair.getFb();
        if (fa.equals(fb)) {
            return weatherIdToString(fa);
        } else {
            StringBuilder sb = new StringBuilder(weatherIdToString(fa));
            sb.append('转');
            sb.append(weatherIdToString(fb));
            return sb.toString();
        }
    }

    public static int weatherIdToDrawable(String weatherId) {
        Integer drawable = drawableLookup.get(weatherId);
        if (drawable == null) {
            return R.drawable.ic_help_outline_black_48dp;
        } else {
            return drawable;
        }
    }

    private WeatherId() {}
}
