package me.erinacio.myweather.vm;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;

import java.util.Arrays;
import java.util.Map;

import me.erinacio.myweather.NaiveLineChart;
import me.erinacio.myweather.R;
import me.erinacio.myweather.api.AqiApiFactory;
import me.erinacio.myweather.api.AqiResult;
import me.erinacio.myweather.api.RequestFactory;

public class AqiViewModule {
    private static final String TAG = "MyWeather";

    private static final int[] aqiDayIds = new int[]{
            R.id.textview_aqi_day1,
            R.id.textview_aqi_day2,
            R.id.textview_aqi_day3,
            R.id.textview_aqi_day4,
            R.id.textview_aqi_day5,
    };

    private static final int[] aqiAqiIds = new int[]{
            R.id.textview_aqi_aqi1,
            R.id.textview_aqi_aqi2,
            R.id.textview_aqi_aqi3,
            R.id.textview_aqi_aqi4,
            R.id.textview_aqi_aqi5,
    };
    
    private static final int[] aqiLevelIds = new int[]{
            R.id.textview_aqi_level1,
            R.id.textview_aqi_level2,
            R.id.textview_aqi_level3,
            R.id.textview_aqi_level4,
            R.id.textview_aqi_level5,
    };
    private final ApiUpdateHelper<AqiResult[]> aqiUpdateHelper;
    private View view;
    private AqiApiFactory apiFactory;
    private RequestQueue requestQueue;
    private String cityName = null;
    private WeatherViewModule.ValueChangedHandler<String> onCityNameChanged = null;

    public AqiViewModule(View view, RequestFactory requestFactory, RequestQueue requestQueue) {
        this.view = view;
        this.apiFactory = new AqiApiFactory(requestFactory);
        this.requestQueue = requestQueue;

        TextView textViewUpdate = this.findViewById(R.id.textview_aqi_update);
        TextView textViewCityName = this.findViewById(R.id.textview_aqi_city);

        aqiUpdateHelper = new ApiUpdateHelper<>(
                view.getContext(),
                textViewUpdate,
                requestQueue,
                AqiResult[].class,
                () -> {
                    Context context = view.getContext();
                    if (cityName == null || cityName.isEmpty()) {
                        cityName = context.getString(R.string.default_cityname);
                    }
                    Log.i(TAG, String.format("AqiViewModule: cityName is %s", cityName));
                },
                (listener, errorListener) -> apiFactory.aqiRequest(cityName, listener, errorListener),
                resultArray -> {
                    Context context = view.getContext();
                    AqiResult result = resultArray[0];

                    AqiResult.CityNow cityNow = result.getCityNow();
                    Map<String, AqiResult.HistoricalData> lastTwoWeeks = result.getLastTwoWeeks();
                    Map<String, AqiResult.MonitorData> lastMoniData = result.getLastMoniData();

                    setTextById(R.id.textview_aqi_city, cityNow.getCity());
                    setTextById(R.id.textview_aqi_realtime, cityNow.getAqi());
                    setTextById(R.id.textview_aqi_level, cityNow.getQuality());
                    textViewUpdate.setText(String.format(context.getString(R.string.updater_updated_fmt), cityNow.getDate()));

                    AqiResult.HistoricalData[] historicalData = lastTwoWeeks.values().toArray(new AqiResult.HistoricalData[lastTwoWeeks.size()]);
                    Arrays.sort(historicalData, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));

                    double[] data = new double[aqiDayIds.length];

                    for (int i = 0; i < aqiDayIds.length; ++i) {
                        AqiResult.HistoricalData datum = historicalData[5 - i];

                        setTextById(aqiDayIds[i], datum.formatDate(context.getString(R.string.common_date_fmt)));
                        setTextById(aqiAqiIds[i], datum.getAqi());
                        setTextById(aqiLevelIds[i], datum.getQuality());
                        data[i] = Double.parseDouble(datum.getAqi());
                    }

                    AqiResult.MonitorData[] monitorData;
                    if (lastMoniData.size() > 0) {
                        monitorData = new AqiResult.MonitorData[lastMoniData.size()];
                        String[] keyArray = lastMoniData.keySet().toArray(new String[monitorData.length]);
                        int[] ikeyArray = new int[monitorData.length];
                        for (int i = 0; i < keyArray.length; ++i) {
                            ikeyArray[i] = Integer.parseInt(keyArray[i]);
                        }
                        Arrays.sort(ikeyArray);
                        for (int i = 0; i < ikeyArray.length; ++i) {
                            monitorData[i] = lastMoniData.get(String.valueOf(ikeyArray[i]));
                        }
                    } else {
                        String emptyPlaceholder = context.getString(R.string.common_empty);
                        monitorData = new AqiResult.MonitorData[1];
                        monitorData[0] = AqiResult.MonitorData.makeEmpty(emptyPlaceholder);
                    }

                    this.<NaiveLineChart>findViewById(R.id.linechart_aqi).setData(data);

                    LinearLayout fakeList = findViewById(R.id.layout_fake_aqi_list);
                    fakeList.removeAllViews();
                    for (AqiResult.MonitorData datum : monitorData) {
                        LayoutInflater inflater = LayoutInflater.from(context);

                        TextView textViewMonitor,
                                textViewAqi,
                                textViewLevel,
                                textViewPm25,
                                textViewPm10;

                        View itemView = inflater.inflate(R.layout.list_aqi_monitor, null);

                        textViewMonitor = itemView.findViewById(R.id.list_textview_aqi_monitor);
                        textViewAqi     = itemView.findViewById(R.id.list_textview_aqi_aqi);
                        textViewLevel   = itemView.findViewById(R.id.list_textview_aqi_level);
                        textViewPm25    = itemView.findViewById(R.id.list_textview_aqi_pm25);
                        textViewPm10    = itemView.findViewById(R.id.list_textview_aqi_pm10);

                        textViewMonitor.setText(datum.getMonitor());
                        textViewAqi    .setText(datum.getAqi());
                        textViewLevel  .setText(datum.getQuality());
                        textViewPm25   .setText(datum.getPm25Hour());
                        textViewPm10   .setText(datum.getPm10Hour());

                        fakeList.addView(itemView);
                    }
                },
                failedResponse -> {
                    Log.e(TAG, String.format("WeatherViewModule: api said %s", failedResponse.getReason()));
                },
                error -> {
                    Log.e(TAG, "AqiViewModule: request failed", error);
                }
        );
    }

    public void updateAqi() {
        aqiUpdateHelper.update();
    }

    @SuppressWarnings("unchecked")
    private <T extends View> T findViewById(int id) {
        View ret = view.findViewById(id);
        return (T) ret;
    }

    private void setTextById(int id, String text) {
        this.<TextView>findViewById(id).setText(text);
    }

    private void setTextById(int id, String text, int defaultId) {
        if (text == null || text.length() == 0) {
            this.<TextView>findViewById(id).setText(defaultId);
        } else {
            this.<TextView>findViewById(id).setText(text);
        }
    }

    private void setTextById(int id, int stringId) {
        this.<TextView>findViewById(id).setText(stringId);
    }

    private void setHtmlById(int id, String html) {
        this.<TextView>findViewById(id).setText(Html.fromHtml(html));
    }

    private void setDrawableById(int id, int drawableId) {
        this.<ImageView>findViewById(id).setImageResource(drawableId);
    }

    public interface ValueChangedHandler<T> {
        void onChanged(T value, T oldValue);
    }

    private interface ErrorInformer {
        void inform(String msg);
    }

    private static class AqiMoniAdapter extends ArrayAdapter<AqiResult.MonitorData> {
        private AqiResult.MonitorData[] data;
        private Context context;

        public AqiMoniAdapter(@NonNull Context context, @NonNull AqiResult.MonitorData[] objects) {
            super(context, R.layout.list_aqi_monitor, objects);
            this.context = context;
            this.data = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            AqiResult.MonitorData datum = data[position];
            LayoutInflater inflater = LayoutInflater.from(getContext());

            TextView textViewMonitor,
                     textViewAqi,
                     textViewLevel,
                     textViewPm25,
                     textViewPm10;

            convertView = inflater.inflate(R.layout.list_aqi_monitor, parent, false);
            convertView.setTag(position);

            textViewMonitor = convertView.findViewById(R.id.list_textview_aqi_monitor);
            textViewAqi     = convertView.findViewById(R.id.list_textview_aqi_aqi);
            textViewLevel   = convertView.findViewById(R.id.list_textview_aqi_level);
            textViewPm25    = convertView.findViewById(R.id.list_textview_aqi_pm25);
            textViewPm10    = convertView.findViewById(R.id.list_textview_aqi_pm10);

            textViewMonitor.setText(datum.getMonitor());
            textViewAqi    .setText(datum.getAqi());
            textViewLevel  .setText(datum.getQuality());
            textViewPm25   .setText(datum.getPm25Hour());
            textViewPm10   .setText(datum.getPm10Hour());

            return convertView;
        }
    }
}
