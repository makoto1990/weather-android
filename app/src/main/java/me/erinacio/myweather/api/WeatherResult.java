package me.erinacio.myweather.api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Locale;

public class WeatherResult {
    public class Realtime {
        private String temp;

        @SerializedName("wind_direction")
        private String windDirection;

        @SerializedName("wind_strength")
        private String windStrength;

        private String humidity;

        private String time;

        public String getTemp() {
            return temp;
        }

        public String getWindDirection() {
            return windDirection;
        }

        public String getWindStrength() {
            return windStrength;
        }

        public String getHumidity() {
            return humidity;
        }

        public String getTime() {
            return time;
        }
    }

    public class WeatherIdPair {
        private String fa;
        private String fb;

        public String getFa() {
            return fa;
        }

        public String getFb() {
            return fb;
        }
    }

    public class Today {
        private String city;

        @SerializedName("date_y")
        private String dateY;

        private String name;

        private String week;

        private String temperature;

        private String weather;

        @SerializedName("weather_id")
        private WeatherIdPair weatherIdPair;

        private String wind;

        @SerializedName("dressing_index")
        private String dressingIndex;

        @SerializedName("dressing_advice")
        private String dressingAdvice;

        @SerializedName("uv_index")
        private String uvIndex;

        @SerializedName("comfort_index")
        private String comfortIndex;

        @SerializedName("wash_index")
        private String washIndex;

        @SerializedName("travel_index")
        private String travelIndex;

        @SerializedName("exercise_index")
        private String exerciseIndex;

        @SerializedName("drying_index")
        private String dryingIndex;

        public String getCity() {
            return city;
        }

        public String getDateY() {
            return dateY;
        }

        public String getName() {
            return name;
        }

        public String getWeek() {
            return week;
        }

        public String getTemperature() {
            return temperature;
        }

        public String getWeather() {
            return weather;
        }

        public WeatherIdPair getWeatherIdPair() {
            return weatherIdPair;
        }

        public String getWind() {
            return wind;
        }

        public String getDressingIndex() {
            return dressingIndex;
        }

        public String getDressingAdvice() {
            return dressingAdvice;
        }

        public String getUvIndex() {
            return uvIndex;
        }

        public String getComfortIndex() {
            return comfortIndex;
        }

        public String getWashIndex() {
            return washIndex;
        }

        public String getTravelIndex() {
            return travelIndex;
        }

        public String getExerciseIndex() {
            return exerciseIndex;
        }

        public String getDryingIndex() {
            return dryingIndex;
        }
    }

    public class Future {
        private static final String TAG = "MyWeather";

        private String temperature;

        private String weather;

        @SerializedName("weather_id")
        private WeatherIdPair weatherIdPair;

        private String wind;

        private String week;

        private String date;

        public String getTemperature() {
            return temperature;
        }

        public String getWeather() {
            return weather;
        }

        public WeatherIdPair getWeatherIdPair() {
            return weatherIdPair;
        }

        public String getWind() {
            return wind;
        }

        public String getWeek() {
            return week;
        }

        public String getDate() {
            return date;
        }

        public String getDateRepr(String format) {
            if (date == null || date.length() < 4) {
                return date;
            }

            int length = date.length();
            String monthStr = date.substring(length - 4, length - 2);
            String dayStr = date.substring(length - 2, length);

            try {
                int month = Integer.parseInt(monthStr);
                int day   = Integer.parseInt(dayStr);
                return String.format(Locale.US, format, String.valueOf(month), String.valueOf(day));
            } catch (NumberFormatException e) {
                Log.w(TAG, "getDateRepr: parseInt failed", e);
                return String.format(Locale.US, format, monthStr, dayStr);
            }
        }
    }

    @SerializedName("sk")
    private Realtime sk;
    private Today today;
    private List<Future> future;

    public Realtime getSk() {
        return sk;
    }

    public Today getToday() {
        return today;
    }

    public List<Future> getFuture() {
        return future;
    }

    public static WeatherResult fromJson(JsonElement element) {
        Gson gson = new Gson();
        return gson.fromJson(element, WeatherResult.class);
    }
}
