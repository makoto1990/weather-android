package me.erinacio.myweather.fragments.dashboard;

import java.io.Serializable;

/**
 * Created by kevin on 2017/12/25.
 */

public class DashboardItem implements Serializable {

    private String title;

    private String content;

    private String date;

    public DashboardItem(String title, String content, String date) {
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public DashboardItem() {
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
