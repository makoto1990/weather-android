package me.erinacio.myweather;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import me.erinacio.myweather.fragments.aqi.AqiRoot;
import me.erinacio.myweather.fragments.calendar.CalendarRoot;
import me.erinacio.myweather.fragments.clock.ClockRoot;
import me.erinacio.myweather.fragments.dashboard.DashboardListAdapter;
import me.erinacio.myweather.fragments.dashboard.DashboardRoot;
import me.erinacio.myweather.fragments.weather.WeatherRoot;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private List<Fragment> fragmentsList;
    private BottomNavigationView navigation;

    private ViewPager.OnPageChangeListener mOnPageChangeListener =new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            Log.i("me.erinacio.myweather", String.format("naive: %d", position));
            switch (position){
                case 0:
                    navigation.setSelectedItemId(R.id.navigation_weather);
                    return;
                case 1:
                    navigation.setSelectedItemId(R.id.navigation_calendar);
                    return;
                case 2:
                    navigation.setSelectedItemId(R.id.navigation_dashboard);
                    return;
                case 3:
                    navigation.setSelectedItemId(R.id.navigation_clock);
                    return;
                case 4:
                    navigation.setSelectedItemId(R.id.navigation_aqi);
                    return;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = (@NonNull MenuItem item) -> {
        switch (item.getItemId()) {
            case R.id.navigation_weather:
                viewPager.setCurrentItem(0);
                return true;
            case R.id.navigation_calendar:
                viewPager.setCurrentItem(1);
                return true;
            case R.id.navigation_dashboard:
                viewPager.setCurrentItem(2);
                return true;
            case R.id.navigation_clock:
                viewPager.setCurrentItem(3);
                return true;
            case R.id.navigation_aqi:
                viewPager.setCurrentItem(4);
                return true;
        }
        return false;
    };

    public void refresh(View view) {
        DashboardRoot.refresh((View) (view.getParent()), () -> {
            DashboardListAdapter dashboardListAdapter = new DashboardListAdapter(DashboardRoot.getDashboardItems(), this);
            ListView listView = findViewById(R.id.metion_list);
            listView.setAdapter(dashboardListAdapter);
        });
    }

    public void gotoFragment(int resultPage) {
        viewPager.setCurrentItem(resultPage);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentsList = new ArrayList<Fragment>();
        fragmentsList.add(new WeatherRoot());
        fragmentsList.add(new CalendarRoot());
        fragmentsList.add(new DashboardRoot());
        fragmentsList.add(new ClockRoot());
        fragmentsList.add(new AqiRoot());
        FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public android.support.v4.app.Fragment getItem(int position) {
                return fragmentsList.get(position);
            }
            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return fragmentsList.size();
            }
        };
        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(mOnPageChangeListener);
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        gotoFragment(2);
    }

}
