package me.erinacio.myweather.vm;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import org.jetbrains.annotations.Nullable;

import me.erinacio.myweather.R;
import me.erinacio.myweather.api.ApiError;
import me.erinacio.myweather.api.ApiRequest;
import me.erinacio.myweather.api.ApiResponse;

final class ApiUpdateHelper<ResultType> {
    private boolean inReloading = false;

    @FunctionalInterface
    public interface Consumer<T> {
        void consume(T data);
    }

    @FunctionalInterface
    public interface Procedure {
        void run();
    }

    @FunctionalInterface
    public interface RequestProvider {
        ApiRequest provide(Response.Listener<ApiResponse> listener, Response.ErrorListener errorListener);
    }

    private final Context context;
    private final TextView textViewUpdate;
    private final RequestQueue requestQueue;
    private final Class<ResultType> resultClass;
    private final RequestProvider requestProvider;
    private final Procedure prepareProcedure;
    private final Consumer<ResultType> dataConsumer;
    private final Consumer<ApiResponse> errorConsumer;
    private final Consumer<VolleyError> fatalErrorConsumer;
    private final Consumer<String> errorInformer;

    public ApiUpdateHelper(Context context,
                           TextView textViewUpdate,
                           RequestQueue requestQueue,
                           Class<ResultType> resultClass,
                           @Nullable
                           Procedure prepareProcedure,
                           RequestProvider requestProvider,
                           @Nullable
                           Consumer<ResultType> dataConsumer,
                           @Nullable
                           Consumer<ApiResponse> errorConsumer,
                           @Nullable
                           Consumer<VolleyError> fatalErrorConsumer) {
        this.context = context;
        this.textViewUpdate = textViewUpdate;
        this.resultClass = resultClass;
        this.requestQueue = requestQueue;
        this.requestProvider = requestProvider;
        this.prepareProcedure = prepareProcedure;
        this.dataConsumer = dataConsumer;
        this.errorConsumer = errorConsumer;
        this.fatalErrorConsumer = fatalErrorConsumer;

        this.textViewUpdate.setOnClickListener((View view) -> {
            update();
        });

        this.errorInformer = (String msg) -> {
            this.textViewUpdate.setText(R.string.updater_update_failed);
            Toast.makeText(this.context, msg, Toast.LENGTH_LONG).show();
            inReloading = false;
        };
    }

    @Nullable
    public ApiRequest update() {
        if (inReloading) return null;

        if (prepareProcedure != null) {
            prepareProcedure.run();
        }

        textViewUpdate.setText(R.string.updater_updating);
        inReloading = true;

        ApiRequest request = requestProvider.provide((ApiResponse response) -> {
            try {
                if (response.getErrorCode() != 0) {
                    errorConsumer.consume(response);
                    errorInformer.consume(ApiError.errorCodeToString(response.getErrorCode()));
                    return;
                }

                ResultType result;
                try {
                    result = new Gson().fromJson(response.getResult(), resultClass);
                } catch (JsonParseException e) {
                    fatalErrorConsumer.consume(new VolleyError(e));
                    errorInformer.consume(context.getString(R.string.updater_update_failed_json));
                    return;
                }

                try {
                    if (dataConsumer != null) {
                        dataConsumer.consume(result);
                    }
                } catch (Exception e) {
                    if (fatalErrorConsumer != null) {
                        fatalErrorConsumer.consume(new VolleyError(e));
                    }
                    errorInformer.consume(e.toString());
                }
            } finally {
                inReloading = false;
            }
        }, (VolleyError e) -> {
            if (fatalErrorConsumer != null) {
                fatalErrorConsumer.consume(e);
            }
            errorInformer.consume(context.getString(R.string.updater_update_failed_network));
        });
        request.addTo(requestQueue);
        return request;
    }
}
