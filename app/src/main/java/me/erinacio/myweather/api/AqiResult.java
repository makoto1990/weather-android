package me.erinacio.myweather.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class AqiResult {
    public static class CityNow {
        private String city;

        @SerializedName("AQI")
        private String aqi;

        private String quality;

        private String date;

        public String getCity() {
            return city;
        }

        public String getAqi() {
            return aqi;
        }

        public String getQuality() {
            return quality;
        }

        public String getDate() {
            return date;
        }

        public String getUpdateTime() {
            String[] parts = date.split("\\s+");
            if (parts.length > 0) {
                return parts[parts.length - 1];
            } else {
                return date;
            }
        }
    }

    public static class HistoricalData {
        private String city;

        @SerializedName("AQI")
        private String aqi;

        private String quality;

        private String date;

        public String getCity() {
            return city;
        }

        public String getAqi() {
            return aqi;
        }

        public String getQuality() {
            return quality;
        }

        public String getDate() {
            return date;
        }

        public String formatDate(String format) {
            String[] parts = date.split("-");
            if (parts.length < 2) {
                return date;
            }
            return String.format(format, parts[parts.length - 2], parts[parts.length - 1]);
        }
    }

    public static class MonitorData {
        @SerializedName("city")
        private String monitor;

        @SerializedName("AQI")
        private String aqi;

        private String quality;

        @SerializedName("PM2.5Hour")
        private String pm25Hour;

        @SerializedName("PM2.5Day")
        private String pm25Day;

        @SerializedName("PM10Hour")
        private String pm10Hour;

        private String lat;

        private String lon;

        public String getMonitor() {
            return monitor;
        }

        public String getAqi() {
            return aqi;
        }

        public String getQuality() {
            return quality;
        }

        public String getPm25Hour() {
            return pm25Hour;
        }

        public String getPm25Day() {
            return pm25Day;
        }

        public String getPm10Hour() {
            return pm10Hour;
        }

        public String getLat() {
            return lat;
        }

        public String getLon() {
            return lon;
        }
        
        public static MonitorData makeEmpty(String placeHolder) {
            MonitorData datum = new MonitorData();
            datum.monitor = placeHolder;
            datum.aqi = placeHolder;
            datum.quality = placeHolder;
            datum.pm25Hour = placeHolder;
            datum.pm25Day = placeHolder;
            datum.pm10Hour = placeHolder;
            datum.lat = placeHolder;
            datum.lon = placeHolder;
            return datum;
        }
    }

    @SerializedName("citynow")
    private CityNow cityNow;

    private Map<String, HistoricalData> lastTwoWeeks;

    private Map<String, MonitorData> lastMoniData;

    public static AqiResult fromJson(JsonElement jsonElement) {
        return new Gson().fromJson(jsonElement, AqiResult.class);
    }

    public CityNow getCityNow() {
        return cityNow;
    }

    public Map<String, HistoricalData> getLastTwoWeeks() {
        return lastTwoWeeks;
    }

    public Map<String, MonitorData> getLastMoniData() {
        return lastMoniData;
    }
}
