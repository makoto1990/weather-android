package me.erinacio.myweather.fragments.aqi;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;

import me.erinacio.myweather.R;
import me.erinacio.myweather.RequestQueueSingleton;
import me.erinacio.myweather.api.ApiAqiRequestFactory;
import me.erinacio.myweather.api.RequestFactory;
import me.erinacio.myweather.vm.AqiViewModule;

/**
 * Created by kevin on 2017/12/19.
 */

public class AqiRoot extends Fragment {
    private RequestFactory requestFactory;
    private RequestQueue requestQueue;

    public static AqiRoot newInstance() {
        AqiRoot fragment = new AqiRoot();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        requestFactory = new ApiAqiRequestFactory();
        requestQueue = RequestQueueSingleton.getInstance(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.aqi, container, false);
        new AqiViewModule(view, requestFactory, requestQueue).updateAqi();
        return view;
    }
}
