package me.erinacio.myweather.vm;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;

import java.util.List;

import me.erinacio.myweather.R;
import me.erinacio.myweather.api.RequestFactory;
import me.erinacio.myweather.api.WeatherApiFactory;
import me.erinacio.myweather.api.WeatherId;
import me.erinacio.myweather.api.WeatherResult;

public class WeatherViewModule {
    private static final String TAG = "MyWeather";


    private static final int[] weatherDayIds = new int[]{
            R.id.textview_weather_day1,
            R.id.textview_weather_day2,
            R.id.textview_weather_day3,
            R.id.textview_weather_day4,
            R.id.textview_weather_day5,
    };

    private static final int[] weatherFaIds = new int[] {
            R.id.imageview_weather_1a,
            R.id.imageview_weather_2a,
            R.id.imageview_weather_3a,
            R.id.imageview_weather_4a,
            R.id.imageview_weather_5a,
    };

    private static final int[] weatherFbIds = new int[] {
            R.id.imageview_weather_1b,
            R.id.imageview_weather_2b,
            R.id.imageview_weather_3b,
            R.id.imageview_weather_4b,
            R.id.imageview_weather_5b,
    };

    private static final int[] weatherNameIds = new int[] {
            R.id.textview_weather_weather1,
            R.id.textview_weather_weather2,
            R.id.textview_weather_weather3,
            R.id.textview_weather_weather4,
            R.id.textview_weather_weather5,
    };

    private static final int[] weatherTempIds = new int[] {
            R.id.textview_weather_temp1,
            R.id.textview_weather_temp2,
            R.id.textview_weather_temp3,
            R.id.textview_weather_temp4,
            R.id.textview_weather_temp5,
    };

    private static final int[] weatherWindIds = new int[] {
            R.id.textview_weather_wind1,
            R.id.textview_weather_wind2,
            R.id.textview_weather_wind3,
            R.id.textview_weather_wind4,
            R.id.textview_weather_wind5,
    };

    private boolean inReloading = false;

    public interface ValueChangedHandler<T> {
        void onChanged(T value, T oldValue);
    }

    private interface ErrorInformer {
        void inform(String msg);
    }

    private View view;
    private WeatherApiFactory apiFactory;
    private RequestQueue requestQueue;
    private String cityName = null;
    private ValueChangedHandler<String> onCityNameChanged = null;
    private ApiUpdateHelper<WeatherResult> weatherUpdateHelper;

    public WeatherViewModule(View view, RequestFactory requestFactory, RequestQueue requestQueue) {
        this.view = view;
        this.apiFactory = new WeatherApiFactory(requestFactory);
        this.requestQueue = requestQueue;

        TextView textViewUpdate = this.findViewById(R.id.textview_weather_update);
        TextView textViewCityName = this.findViewById(R.id.textview_weather_cityname);

        weatherUpdateHelper = new ApiUpdateHelper<>(
                view.getContext(),
                textViewUpdate,
                requestQueue,
                WeatherResult.class,
                () -> {
                    Context context = view.getContext();
                    if (cityName == null || cityName.isEmpty()) {
                        cityName = context.getString(R.string.default_cityname);
                    }
                    Log.i(TAG, String.format("WeatherViewModule: cityName is %s", cityName));
                },
                (listener, errorListener) -> apiFactory.weatherRequest(cityName, listener, errorListener),
                result -> {
                    Context context = view.getContext();

                    WeatherResult.Realtime   realtime = result.getSk();
                    WeatherResult.Today         today = result.getToday();
                    List<WeatherResult.Future> future = result.getFuture();

                    textViewCityName.setText(today.getCity());
                    textViewUpdate.setText(String.format(context.getString(R.string.updater_updated_fmt), realtime.getTime()));
                    setTextById(R.id.textview_weather_temp, String.format(context.getString(R.string.weather_temp_fmt), realtime.getTemp()));
                    setHtmlById(R.id.textview_weather_realtime,
                            String.format(context.getString(R.string.weather_realtime_fmt),
                                    realtime.getWindDirection(),
                                    realtime.getWindStrength(),
                                    realtime.getHumidity()));

                    String dateFmt = context.getString(R.string.common_date_fmt);
                    for (int i = 0; i < weatherDayIds.length; ++i) {
                        if (i < future.size()) {
                            WeatherResult.Future weather = future.get(i);
                            setTextById(weatherDayIds[i], weather.getDateRepr(dateFmt));
                            setDrawableById(weatherFaIds[i], WeatherId.weatherIdToDrawable(weather.getWeatherIdPair().getFa()));
                            setDrawableById(weatherFbIds[i], WeatherId.weatherIdToDrawable(weather.getWeatherIdPair().getFb()));
                            setTextById(weatherNameIds[i], WeatherId.widPairToString(weather.getWeatherIdPair()));
                            setTextById(weatherTempIds[i], weather.getTemperature());
                            setTextById(weatherWindIds[i], weather.getWind());
                        } else {
                            setTextById(weatherDayIds[i], R.string.common_empty);
                            setDrawableById(weatherFaIds[i], R.drawable.ic_help_outline_black_48dp);
                            setDrawableById(weatherFbIds[i], R.drawable.ic_help_outline_black_48dp);
                            setTextById(weatherNameIds[i], R.string.common_empty);
                            setTextById(weatherTempIds[i], R.string.common_empty);
                            setTextById(weatherWindIds[i], R.string.common_empty);
                        }
                    }

                    setTextById(R.id.textview_weather_dressing_index,
                            today.getDressingIndex(),
                            R.string.common_empty);
                    setTextById(R.id.textview_weather_dressing_advice,
                            today.getDressingAdvice(),
                            R.string.common_empty);
                    setTextById(R.id.textview_weather_uv_index,
                            today.getUvIndex(),
                            R.string.common_empty);
                    setTextById(R.id.textview_weather_comfort_index,
                            today.getComfortIndex(),
                            R.string.common_empty);
                    setTextById(R.id.textview_weather_wash_index,
                            today.getWashIndex(),
                            R.string.common_empty);
                    setTextById(R.id.textview_weather_travel_index,
                            today.getTravelIndex(),
                            R.string.common_empty);
                    setTextById(R.id.textview_weather_exercise_index,
                            today.getExerciseIndex(),
                            R.string.common_empty);
                    setTextById(R.id.textview_weather_drying_index,
                            today.getDryingIndex(),
                            R.string.common_empty);
                },
                failedResponse -> {
                    Log.e(TAG, String.format("WeatherViewModule: api said %s", failedResponse.getReason()));
                },
                error -> {
                    Log.e(TAG, "WeatherViewModule: request failed", error);
                }
            );
    }

    public void updateWeather() {
        weatherUpdateHelper.update();
    }


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        boolean changing;
        if (cityName != null) {
            changing = !cityName.equals(this.cityName);
        } else {
            changing = (this.cityName != null);
        }
        String oldCityName = this.cityName;
        this.cityName = cityName;
        if (changing && onCityNameChanged != null) {
            onCityNameChanged.onChanged(cityName, oldCityName);
        }
    }

    public ValueChangedHandler<String> getOnCityNameChanged() {
        return onCityNameChanged;
    }

    public void setOnCityNameChanged(ValueChangedHandler<String> onCityNameChanged) {
        this.onCityNameChanged = onCityNameChanged;
    }

    @SuppressWarnings("unchecked")
    private <T extends View> T findViewById(int id) {
        View ret = view.findViewById(id);
        return (T) ret;
    }

    private void setTextById(int id, String text) {
        this.<TextView>findViewById(id).setText(text);
    }

    private void setTextById(int id, String text, int defaultId) {
        if (text == null || text.length() == 0) {
            this.<TextView>findViewById(id).setText(defaultId);
        } else {
            this.<TextView>findViewById(id).setText(text);
        }
    }

    private void setTextById(int id, int stringId) {
        this.<TextView>findViewById(id).setText(stringId);
    }

    private void setHtmlById(int id, String html) {
        this.<TextView>findViewById(id).setText(Html.fromHtml(html));
    }

    private void setDrawableById(int id, int drawableId) {
        this.<ImageView>findViewById(id).setImageResource(drawableId);
    }
}
