package me.erinacio.myweather.api;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class RequestFactory {
    @NotNull private final                  String prefix;
    @NotNull private final HashMap<String, Object> preludes;

    public RequestFactory(@NotNull                  String prefix,
                          @NotNull HashMap<String, Object> preludes) {
        this.  prefix = prefix;
        this.preludes = preludes;
    }

    public ApiRequest makeApiRequest(String apiUrl,
                                     Map<String, ?> params,
                                     Response.Listener<ApiResponse> listener,
                                     Response.ErrorListener errorListener) {
        StringBuilder sb = new StringBuilder(prefix);
        if (!prefix.endsWith("/") && !apiUrl.startsWith("/")) {
            sb.append('/');
        } else if (prefix.endsWith("/") && apiUrl.startsWith("/")) {
            sb.setLength(sb.length() - 1);
        }
        sb.append(apiUrl);

        HashMap<String, Object> newParams = new HashMap<>(preludes);
        newParams.putAll(params);

        return ApiRequest.makeRequest(sb.toString(), newParams, listener, errorListener);
    }
}
