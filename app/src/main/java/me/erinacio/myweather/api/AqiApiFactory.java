package me.erinacio.myweather.api;

import com.android.volley.Response;

import java.util.HashMap;

public class AqiApiFactory {
    private static final String urlWeather = "/environment/air/cityair";
    private RequestFactory requestFactory;

    public AqiApiFactory(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    public ApiRequest aqiRequest(String city,
                                 Response.Listener<ApiResponse> listener,
                                 Response.ErrorListener errorListener) {
        return requestFactory.makeApiRequest(urlWeather, new HashMap<String, Object>() {
            {
                put("city", city);
            }
        }, listener, errorListener);
    }
}
