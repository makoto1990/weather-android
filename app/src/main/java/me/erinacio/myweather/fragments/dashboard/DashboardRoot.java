package me.erinacio.myweather.fragments.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import me.erinacio.myweather.DashboardContentActivity;
import me.erinacio.myweather.MainActivity;
import me.erinacio.myweather.R;
import me.erinacio.myweather.RequestQueueSingleton;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by kevin on 2017/12/19.
 */

public class DashboardRoot extends Fragment {

    private static List<DashboardItem> dashboardItems = new ArrayList<>();
    private static Gson gson = new Gson();
    private DashboardListAdapter dashboardListAdapter;

    public static List<DashboardItem> getDashboardItems() {
        return dashboardItems;
    }

    public static DashboardRoot newInstance() {
        DashboardRoot fragment = new DashboardRoot();
        return fragment;
    }

    public interface Fucker {
        void fuck();
    }

    public static void refresh(View view, @NotNull Fucker fucker) {
        RequestQueue requestQueue = RequestQueueSingleton.getInstance(view.getContext());
        String url = "http://cn2.vxtrans.link:39268/api/fetch-all";
        Toast loadingToast = Toast.makeText(view.getContext(), "加载中，请稍等", Toast.LENGTH_LONG);
        loadingToast.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, (JSONArray response) -> {
                    dashboardItems = gson.fromJson(response.toString(), new TypeToken<List<DashboardItem>>() {
                    }.getType());
                    fucker.fuck();
                    loadingToast.cancel();
                    Toast.makeText(view.getContext(), "加载成功", Toast.LENGTH_SHORT).show();

                }, (VolleyError error) -> {
                    Log.e(TAG, "DashboardView: request failed", error);
                    loadingToast.cancel();
                    Toast.makeText(view.getContext(), "加载失败，请稍候重试或检查网络连接", Toast.LENGTH_SHORT).show();
                });
        requestQueue.add(jsonArrayRequest);
    }

    public static void refresh(View view) {
        refresh(view, () -> {});
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 0) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.gotoFragment(2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.dashboard, container, false);
        refresh(view);
        ListView listView = view.findViewById(R.id.metion_list);
        listView.setOnItemClickListener((AdapterView<?> parent, View view1, int position, long id) -> {
            Intent intent = new
                    Intent(getActivity(), DashboardContentActivity.class);
            //在Intent对象当中添加一个键值对
            intent.putExtra("dashboardItem", dashboardItems.get(position));
            startActivityForResult(intent, 1);
        });

        view.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {

            @Override
            public void onViewAttachedToWindow(View v) {
                refresh(view, () -> {
                    dashboardListAdapter = new DashboardListAdapter(dashboardItems, getActivity());
                    listView.setAdapter(dashboardListAdapter);
                });
            }

            @Override
            public void onViewDetachedFromWindow(View v) {

            }
        });

        return view;
    }
}
